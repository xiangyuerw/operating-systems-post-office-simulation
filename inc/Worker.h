/*
 * Worker.h
 *
 *  Created on: Mar 19, 2015
 *      Author: xiangyuwang
 */

#ifndef WORKER_H_
#define WORKER_H_

class Worker {
private:
	int workerid;
	int customerid;
	int serviceid;
public:
	Worker();
	Worker(int id);
	void start();
	virtual ~Worker();
};

#endif /* WORKER_H_ */
