/*
 * OSProject2.h
 *
 *  Created on: Mar 19, 2015
 *      Author: xiangyuwang
 */

#ifndef OSPROJECT2_H_
#define OSPROJECT2_H_
#include <semaphore.h>
#include <queue>
using namespace std;

#define MAX_NUMBER_CUSTOMER 50
#define MAX_NUMBER_WORKER 3
#define MAX_NUMBER_SCALE 1
#define MAX_NUMBER_SERVICE 3
#define MAX_IN_OFFICE 10
extern char serviceName[MAX_NUMBER_SERVICE][15];
//extern char serviceName;
extern sem_t enter;   //MAX_IN_OFFICE
extern sem_t ready;
extern sem_t serve;    //MAX_NUMBER_WORKERS
extern sem_t scale;    //binary
extern sem_t served;
//extern sem_t print_served;
extern sem_t servicetype;
extern sem_t finish[MAX_NUMBER_CUSTOMER];
extern int worker_customer[MAX_NUMBER_WORKER];
extern sem_t cust_critical;
extern sem_t worker_critical;
extern sem_t print;
extern int s_serviceid;
extern int s_customerid;
extern int s_workerid;
extern int custome_served;
extern queue<int> customeridque;




#endif /* OSPROJECT2_H_ */
