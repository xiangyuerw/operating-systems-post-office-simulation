/*
 * Customer.h
 *
 *  Created on: Mar 19, 2015
 *      Author: xiangyuwang
 */

#ifndef CUSTOMER_H_
#define CUSTOMER_H_

class Customer {
private:
	int customerid;
	int serviceid;

	int workerid;
	//create task randomly

public:
	Customer();
	Customer(int id);
	void start();
	virtual ~Customer();
};

#endif /* CUSTOMER_H_ */
