//============================================================================
// Name        : OSProject2.cpp
// Author      : Xiangyu Wang
// Version     :
// Copyright   : This is created by Xiangyu Wang
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <pthread.h>
#include "OSProject2.h"
#include "Customer.h"
#include "Worker.h"
#include <stdlib.h>
//using namespace std;
sem_t enter;
sem_t serve;
sem_t ready;
sem_t scale;
sem_t served;
//sem_t print_served;
sem_t servicetype;
sem_t finish[MAX_NUMBER_CUSTOMER];
sem_t print;
sem_t cust_critical;
sem_t worker_critical;
int s_serviceid = 0;
int s_customerid = 0;
//int s_workerid = 0;
int worker_customer[MAX_NUMBER_WORKER]={3,3,3};
char serviceName[MAX_NUMBER_SERVICE][15] = {{"buy stamp"}, {"mail letter"}, {"mail package"}};
int custome_served = 0;
queue<int> customeridque;
Customer customer[MAX_NUMBER_CUSTOMER];
Worker worker[MAX_NUMBER_WORKER];
void createSemaphores();
void closeSemaphores();
void *Customer_thread_handler(void *p);
void *Worker_thread_handler(void *p);
int main() {

	pthread_t pCus_thread[MAX_NUMBER_CUSTOMER];

	pthread_t pWorker_thread[MAX_NUMBER_WORKER];
	int i = 0;
	int errCode = 0;
	cout << "OSProject2 by Xiangyu Wang!" << endl; // prints !!!Hello Xiangyu wang!!
	srand((unsigned)time(NULL));
	createSemaphores();
	for(i = 0; i < MAX_NUMBER_CUSTOMER; i++)
	{

		sem_wait(&print);
		cout<<"Customer "<<i<<" is created!"<<endl;
		sem_post(&print);
		if(errCode = pthread_create(&pCus_thread[i], NULL, Customer_thread_handler, &i ))
			exit(1);
		//errexit(errCode, "pthread_creat)");
	}
	for(i = 0; i < MAX_NUMBER_WORKER; i++)
	{

		sem_wait(&print);
		cout<<"Worker "<<i<<" is created!"<<endl;
		sem_post(&print);
		pthread_create(&(pWorker_thread[i]), NULL, Worker_thread_handler, &i);
	}
	for(i = 0; i < MAX_NUMBER_CUSTOMER; i++)
	{
		if(errCode = pthread_join(pCus_thread[i], NULL))
			exit(1);
			//errexit(errcode, "pthread_join");
		sem_wait(&print);
		cout<<"Joined customer "<<i<<endl;
		sem_post(&print);

	}
	for(i = 0; i<MAX_NUMBER_WORKER; i++)//????
	{
		if(errCode = pthread_join(pWorker_thread[i], NULL))
		exit(1);
		//errexit(errCode, "pthread_join");

	}
	closeSemaphores();
	return 0;
}

void *Customer_thread_handler(void *p)
{
	Customer c((int)*p);
	customer[(int)*p] = c;
	//cout<<"Customer_thread_handler"<<endl;
	customer[(int)*p].start();
}
void *Worker_thread_handler(void *p)
{
	Worker w(*(int*)p);
	worker[(int)*p] = w;
	//cout<<"Worker_thread_handler"<<endl;
	worker[(int)*p].start();
}
void createSemaphores()
{
	sem_init(&enter, 0, MAX_IN_OFFICE);
	sem_init(&ready, 0, 0);
	sem_init(&serve, 0, MAX_NUMBER_WORKER);
	sem_init(&served, 0, 0);
	sem_init(&scale, 0, 1);

	sem_init(&cust_critical, 0, 1);
	sem_init(&worker_critical, 0, 1);
	sem_init(&print, 0, 1);
	//sem_init(&print_served, 0, 0);
	sem_init(&servicetype, 0, 0);
	for(int i = 0; i<MAX_NUMBER_CUSTOMER; i++)
	{
		sem_init(&finish[i], 0, 0);
	}

}
void closeSemaphores()
{
	sem_close(&enter);
	sem_close(&ready);
	sem_close(&serve);
	sem_close(&served);
	sem_close(&scale);
	sem_close(&cust_critical);
	sem_close(&worker_critical);
	sem_close(&print);
	sem_close(&servicetype);
	for(int i = 0; i<MAX_NUMBER_CUSTOMER; i++)
		sem_close(&finish[i]);

}
