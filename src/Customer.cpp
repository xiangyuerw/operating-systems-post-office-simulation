/*
 * Customer.cpp
 *
 *  Created on: Mar 19, 2015
 *      Author: xiangyuwang
 */

#include "Customer.h"
#include <iostream>
#include <stdlib.h>
#include <sstream>
#include "OSProject2.h"
//using namespace std;
Customer::Customer() {
	// TODO Auto-generated constructor stub

}
Customer::Customer(int id){
	customerid = id;
	//create task randomly
	//srand((unsigned)time(NULL));
	serviceid = rand()%(MAX_NUMBER_SERVICE);
	//cout<<"customer service id = "<<serviceid<<endl;
	//sem_wait(&print);
	//cout<<"Customer "<<customerid<<" is created!"<<endl;
	//sem_post(&print);
}
void Customer::start(){
	bool result = false;

	sem_wait(&enter);
	sem_wait(&print);
	cout<<"Customer "<<customerid<<" enters post office"<<endl;
	sem_post(&print);
	sem_wait(&serve);

	sem_wait(&cust_critical);
	//s_customerid = customerid;
	customeridque.push(customerid);
	customeridque.push(serviceid);
	sem_post(&ready);
	sem_post(&cust_critical);

	sem_wait(&served);
	//workerid = s_workerid;
	while(true)
	{
		for(int i = 0; i<MAX_NUMBER_WORKER; i++)
		{
			if(worker_customer[i] == customerid)
			{
				workerid = i;
				result = true;
			}
		}
		if(result == true)
			break;
	}

	sem_wait(&print);
	cout<<"Customer "<<customerid<<" asks worker "<<workerid<<" to "<< serviceName[serviceid]<<endl; //service name;
	sem_post(&print);
	sem_post(&servicetype);


	sem_wait(&finish[customerid]);
	sem_wait(&print);
	cout<<"Customer "<<customerid<<" finished to "<<serviceName[serviceid]<<endl; //service name;
	sem_post(&print);
	sem_wait(&print);
	cout<<"Customer "<<customerid<<" leaves post office"<<endl;
	sem_post(&print);
	sem_post(&enter);


}
Customer::~Customer() {
	// TODO Auto-generated destructor stub
}

