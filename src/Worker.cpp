/*
 * Worker.cpp
 *
 *  Created on: Mar 19, 2015
 *      Author: xiangyuwang
 */

#include "Worker.h"
#include "OSProject2.h"
#include <time.h>
#include <semaphore.h>
#include <iostream>
#include <unistd.h>
//using namespace std;

Worker::Worker() {
	// TODO Auto-generated constructor stub

}
Worker::Worker(int id){
	workerid = id;
	//sem_wait(&print);
	//cout<<"Worker "<<workerid<<" is created!"<<endl;
	//sem_post(&print);
}
void Worker::start(){

	while(custome_served < MAX_NUMBER_CUSTOMER)
	{

		sem_wait(&ready);
		sem_wait(&worker_critical);
		//s_workerid = workerid;
		//serviceid = s_serviceid;
		//customerid = s_customerid;
		customerid = (int)customeridque.front();
		customeridque.pop();
		serviceid = (int)customeridque.front();
		customeridque.pop();
		sem_post(&worker_critical);
		sem_wait(&print);
		cout<<"Worker "<<workerid<< " is serving customer " <<customerid <<endl;
		sem_post(&print);

		sem_wait(&worker_critical);
		worker_customer[workerid] = customerid;
		sem_post(&served);
		sem_post(&worker_critical);

		sem_wait(&servicetype);

		if(serviceid == 0)
		{
			sleep(1);//60,120,...
		}
		else if(serviceid == 1)
		{
			timespec t_req = {0};
			timespec t_rem = {0};
			t_req.tv_sec = 1;
			t_req.tv_nsec = 1500000000;
			nanosleep(&t_req, &t_rem);
		}
		if(serviceid == 2)
		{
			sem_wait(&scale);
			sem_wait(&print);
			cout<<"Worker "<<workerid<<" is using scale"<<endl;
			sem_post(&print);
			sleep(2);
			sem_wait(&print);
			cout<<"Scale is released by worker "<<workerid<<endl;
			sem_post(&print);
			sem_post(&scale);
		}
		custome_served++;
		sem_wait(&print);
		cout<<"Worker "<<workerid<<" finished to serve customer "<<customerid<<endl;
		sem_post(&print);
		sem_post(&finish[customerid]);
		sem_post(&serve);
	}

}
Worker::~Worker() {
	// TODO Auto-generated destructor stub
}

